﻿using CartolaAplicacao.DAO;
using CartolaAplicacao.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web.Helpers;
using System.Web.Script.Serialization;

namespace CartolaAplicacao
{
    class Program
    {
        static void Main(string[] args)
        {
            //PuxarClubes();
            PuxarAtletas();
        }

        private static void PuxarClubes()
        {
            string URLAuth = "https://api.cartolafc.globo.com/clubes";


            const string contentType = "application/x-www-form-urlencoded";
            System.Net.ServicePointManager.Expect100Continue = false;

            HttpWebRequest webRequest = WebRequest.Create(URLAuth) as HttpWebRequest;
            webRequest.Method = "GET";
            webRequest.ContentType = contentType;
            webRequest.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";

            StreamReader responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream());
            string responseData = responseReader.ReadToEnd();

            var serializer = new JavaScriptSerializer();
            dynamic data = serializer.Deserialize(responseData, typeof(object));

            List<Clubes> lstClubes = new List<Clubes>();

            foreach (var item in data)
            {
                Clubes c = new Clubes();

                foreach (var value in item.Value)
                {
                    var valor = value.Value;
                    var chave = value.Key;

                    if (chave == "id")
                        c.Id = valor.ToString();
                    if (chave == "nome")
                        c.Nome = valor.ToString();
                    if (chave == "abreviacao")
                        c.Abreviacao = valor.ToString();
                }

                lstClubes.Add(c);
            }

            Insert_Data insertData = new Insert_Data();
            insertData.InsertClubeData(lstClubes);

            responseReader.Close();
            webRequest.GetResponse().Close();
        }

        private static void PuxarAtletas()
        {
            string URLAuth = "https://api.cartolafc.globo.com/atletas/mercado";


            const string contentType = "application/x-www-form-urlencoded";
            System.Net.ServicePointManager.Expect100Continue = false;

            HttpWebRequest webRequest = WebRequest.Create(URLAuth) as HttpWebRequest;
            webRequest.Method = "GET";
            webRequest.ContentType = contentType;
            webRequest.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8";

            StreamReader responseReader = new StreamReader(webRequest.GetResponse().GetResponseStream());
            string responseData = responseReader.ReadToEnd();

            var serializer = new JavaScriptSerializer();
            dynamic data = serializer.Deserialize(responseData, typeof(object));

            List<Atleta> lstAtleta = new List<Atleta>();
            List<Scout> lstScout = new List<Scout>();

            foreach (var item in data)
            {
                if (item.Key == "atletas")
                {
                    foreach (var value in item.Value)
                    {
                        Atleta a = new Atleta();

                        foreach (var v in value)
                        {
                            var chave = v.Key;
                            var valor = v.Value;

                            if (chave == "nome")
                                a.nome = valor.ToString();
                            if (chave == "apelido")
                                a.apelido = valor.ToString();
                            if (chave == "atleta_id")
                                a.atletaid = valor.ToString();
                            if (chave == "rodada_id")
                                a.rodada = valor.ToString();
                            if (chave == "clube_id")
                                a.clube = valor.ToString();
                            if (chave == "posicao_id")
                                a.posicao = valor.ToString();
                            if (chave == "status_id")
                                a.status = valor.ToString();
                            if (chave == "pontos_num")
                                a.pontos = valor.ToString();
                            if (chave == "preco_num")
                                a.preco = valor.ToString();
                            if (chave == "variacao_num")
                                a.variacao = valor.ToString();
                            if (chave == "media_num")
                                a.media = valor.ToString();
                            if (chave == "jogos_num")
                                a.jogos = valor.ToString();

                            if (chave == "partida")
                            {
                                foreach (var i in valor)
                                {
                                    if (i.Key == "clube_casa_id")
                                        a.clubecasa = i.Value.ToString();
                                    if (i.Key == "clube_visitante_id")
                                        a.clubevisitante = i.Value.ToString();
                                }
                            }

                            if (chave == "scout")
                            {
                                foreach (var i in valor)
                                {
                                    Scout s = new Scout();
                                    s.AtletaId = a.atletaid;
                                    s.RodadaId = a.rodada;
                                    s.ScoutNome = i.Key.ToString();
                                    s.ScoutValor = i.Value.ToString();
                                    lstScout.Add(s);
                                }
                            }

                            lstAtleta.Add(a);
                        }
                    }

                }

                Insert_Data insertData = new Insert_Data();
                //insertData.InsertAtletaData(lstAtleta);
                insertData.InsertScoutData(lstScout);

                responseReader.Close();
                webRequest.GetResponse().Close();
            }
        }

        
    }
}
