﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CartolaAplicacao.Model
{
    public class Clubes
    {
        public string Id { get; set; }
        public string Nome { get; set; }
        public string Abreviacao { get; set; }
    }
}
