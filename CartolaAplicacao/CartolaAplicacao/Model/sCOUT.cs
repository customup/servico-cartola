﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CartolaAplicacao.Model
{
    public class Scout
    {
        public string AtletaId { get; set; }
        public string RodadaId { get; set; }
        public string ScoutNome { get; set; }
        public string ScoutValor { get; set; }
    }
}
