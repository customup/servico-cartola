﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CartolaAplicacao.Model
{
    public class Atleta
    {
        public string nome { get; set; }
        public string apelido { get; set; }
        public string atletaid { get; set; }
        public string rodada { get; set; }
        public string clube { get; set; }
        public string posicao { get; set; }
        public string status { get; set; }
        public string pontos { get; set; }
        public string preco { get; set; }
        public string variacao { get; set; }
        public string media { get; set; }
        public string jogos { get; set; }
        public string clubecasa { get; set; }
        public string clubevisitante { get; set; }
    }
}
