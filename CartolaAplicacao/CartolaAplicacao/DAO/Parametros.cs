﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CartolaAplicacao.DAO
{
    public class Parametros
    {
        public string parameterName { get; set; }
        public SqlDbType sqlDbType { get; set; }
        public ParameterDirection direction { get; set; }
        public string value { get; set; }
    }
}
