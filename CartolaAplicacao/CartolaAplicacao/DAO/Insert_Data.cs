﻿using CartolaAplicacao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CartolaAplicacao.DAO
{
    public class Insert_Data
    {
        Db_Database dbDatabase = new Db_Database();
        string connectionString = @"Server=.\sql2012;Database=Banco_Cartola;Uid=sa;Pwd=fortalez@2010;";

        public void InsertClubeData(List<Clubes> lstClubes)
        {
            foreach (Clubes c in lstClubes)
            {
                List<Parametros> lstParametros = new List<Parametros>();

                Parametros p = new Parametros();
                p.parameterName = "Id";
                p.sqlDbType = System.Data.SqlDbType.VarChar;
                p.direction = System.Data.ParameterDirection.Input;
                p.value = c.Id;

                lstParametros.Add(p);

                p = new Parametros();
                p.parameterName = "Nome";
                p.sqlDbType = System.Data.SqlDbType.VarChar;
                p.direction = System.Data.ParameterDirection.Input;
                p.value = c.Nome;

                lstParametros.Add(p);

                p = new Parametros();
                p.parameterName = "Abreviacao";
                p.sqlDbType = System.Data.SqlDbType.VarChar;
                p.direction = System.Data.ParameterDirection.Input;
                p.value = c.Abreviacao;

                lstParametros.Add(p);

                dbDatabase.ExecutaProcedure("Origin.InsertClubes", connectionString, lstParametros);
            }            
        }

        public void InsertAtletaData(List<Atleta> lstAtleta)
        {
            foreach (Atleta a in lstAtleta)
            {
                List<Parametros> lstParametros = new List<Parametros>();

                Parametros p = new Parametros();
                p.parameterName = "nome";
                p.sqlDbType = System.Data.SqlDbType.VarChar;
                p.direction = System.Data.ParameterDirection.Input;
                p.value = a.nome;

                lstParametros.Add(p);

                p = new Parametros();
                p.parameterName = "apelido";
                p.sqlDbType = System.Data.SqlDbType.VarChar;
                p.direction = System.Data.ParameterDirection.Input;
                p.value = a.apelido;

                lstParametros.Add(p);

                p = new Parametros();
                p.parameterName = "atletaid";
                p.sqlDbType = System.Data.SqlDbType.VarChar;
                p.direction = System.Data.ParameterDirection.Input;
                p.value = a.atletaid;

                lstParametros.Add(p);

                p = new Parametros();
                p.parameterName = "rodada";
                p.sqlDbType = System.Data.SqlDbType.VarChar;
                p.direction = System.Data.ParameterDirection.Input;
                p.value = a.rodada;

                lstParametros.Add(p);

                p = new Parametros();
                p.parameterName = "clube";
                p.sqlDbType = System.Data.SqlDbType.VarChar;
                p.direction = System.Data.ParameterDirection.Input;
                p.value = a.clube;

                lstParametros.Add(p);

                p = new Parametros();
                p.parameterName = "posicao";
                p.sqlDbType = System.Data.SqlDbType.VarChar;
                p.direction = System.Data.ParameterDirection.Input;
                p.value = a.posicao;

                lstParametros.Add(p);

                p = new Parametros();
                p.parameterName = "status";
                p.sqlDbType = System.Data.SqlDbType.VarChar;
                p.direction = System.Data.ParameterDirection.Input;
                p.value = a.status;

                lstParametros.Add(p);

                p = new Parametros();
                p.parameterName = "pontos";
                p.sqlDbType = System.Data.SqlDbType.VarChar;
                p.direction = System.Data.ParameterDirection.Input;
                p.value = a.pontos;

                lstParametros.Add(p);

                p = new Parametros();
                p.parameterName = "preco";
                p.sqlDbType = System.Data.SqlDbType.VarChar;
                p.direction = System.Data.ParameterDirection.Input;
                p.value = a.preco;

                lstParametros.Add(p);

                p = new Parametros();
                p.parameterName = "variacao";
                p.sqlDbType = System.Data.SqlDbType.VarChar;
                p.direction = System.Data.ParameterDirection.Input;
                p.value = a.variacao;

                lstParametros.Add(p);

                p = new Parametros();
                p.parameterName = "media";
                p.sqlDbType = System.Data.SqlDbType.VarChar;
                p.direction = System.Data.ParameterDirection.Input;
                p.value = a.media;

                lstParametros.Add(p);

                p = new Parametros();
                p.parameterName = "jogos";
                p.sqlDbType = System.Data.SqlDbType.VarChar;
                p.direction = System.Data.ParameterDirection.Input;
                p.value = a.jogos;

                lstParametros.Add(p);

                p = new Parametros();
                p.parameterName = "clubecasa";
                p.sqlDbType = System.Data.SqlDbType.VarChar;
                p.direction = System.Data.ParameterDirection.Input;
                p.value = a.clubecasa;

                lstParametros.Add(p);

                p = new Parametros();
                p.parameterName = "clubevisitante";
                p.sqlDbType = System.Data.SqlDbType.VarChar;
                p.direction = System.Data.ParameterDirection.Input;
                p.value = a.clubevisitante;

                lstParametros.Add(p);

                dbDatabase.ExecutaProcedure("Origin.InsertAtleta", connectionString, lstParametros);
            }
        }

        public void InsertScoutData(List<Scout> lstScout)
        {
            foreach (Scout a in lstScout)
            {
                List<Parametros> lstParametros = new List<Parametros>();

                Parametros p = new Parametros();

                p = new Parametros();
                p.parameterName = "atletaid";
                p.sqlDbType = System.Data.SqlDbType.VarChar;
                p.direction = System.Data.ParameterDirection.Input;
                p.value = a.AtletaId;

                lstParametros.Add(p);

                p = new Parametros();
                p.parameterName = "rodada";
                p.sqlDbType = System.Data.SqlDbType.VarChar;
                p.direction = System.Data.ParameterDirection.Input;
                p.value = a.RodadaId;

                lstParametros.Add(p);

                p = new Parametros();
                p.parameterName = "nome";
                p.sqlDbType = System.Data.SqlDbType.VarChar;
                p.direction = System.Data.ParameterDirection.Input;
                p.value = a.ScoutNome;

                lstParametros.Add(p);

                p = new Parametros();
                p.parameterName = "valor";
                p.sqlDbType = System.Data.SqlDbType.VarChar;
                p.direction = System.Data.ParameterDirection.Input;
                p.value = a.ScoutValor;

                lstParametros.Add(p);

                dbDatabase.ExecutaProcedure("Origin.InsertScout", connectionString, lstParametros);
            }
        }

     }
}
