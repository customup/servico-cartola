﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace CartolaAplicacao.DAO
{
    public class Db_Database
    {
        #region [ Database Communication ]
        public void ExecuteNonQuery(string comando, string stringConnection)
        {
            SqlConnection connection = new SqlConnection(stringConnection);
            SqlCommand cmd;
            connection.Open();

            try
            {
                cmd = connection.CreateCommand();
                cmd.CommandText = comando;
                cmd.ExecuteNonQuery();

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }


            }
        }

        public DataTable ExecutaComandoToDataTable(string comando, string connectionString)
        {
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand cmd;
            connection.Open();

            try
            {
                cmd = connection.CreateCommand();
                cmd.CommandText = comando;
                SqlDataAdapter adap = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adap.Fill(ds);
                DataTable tabelaDados = ds.Tables[0];

                return tabelaDados;
            }
            catch (Exception)
            {
                return new DataTable();
            }
            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

        }

        public string ExecutaComando(string comando, string stringConnection)
        {
            SqlConnection connection = new SqlConnection(stringConnection);
            SqlCommand cmd;
            connection.Open();

            try
            {
                cmd = connection.CreateCommand();
                cmd.CommandText = comando;
                SqlDataAdapter adap = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adap.Fill(ds);
                DataTable tabelaDados = ds.Tables[0];

                JavaScriptSerializer serializer = new JavaScriptSerializer();
                List<Dictionary<string, object>> rows = new List<Dictionary<string, object>>();
                Dictionary<string, object> row;
                foreach (DataRow dr in tabelaDados.Rows)
                {
                    row = new Dictionary<string, object>();
                    foreach (DataColumn col in tabelaDados.Columns)
                    {
                        row.Add(col.ColumnName, dr[col]);
                    }
                    rows.Add(row);
                }

                return serializer.Serialize(rows);

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }


            }

        }

        public string ExecutaProcedure(string nameProcedure, string stringConnection, List<Parametros> lstParametros)
        {
            SqlConnection connection = new SqlConnection(stringConnection);
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;

            try
            {
                cmd.CommandText = nameProcedure;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection = connection;

                foreach (Parametros item in lstParametros)
                {
                    SqlParameter parameter = new SqlParameter();
                    parameter.ParameterName = item.parameterName;
                    parameter.SqlDbType = item.sqlDbType;
                    parameter.Direction = item.direction;
                    parameter.Value = item.value;

                    cmd.Parameters.Add(parameter);
                }

                connection.Open();

                reader = cmd.ExecuteReader();
            }
            catch (Exception ex)
            {
            }
            finally
            {
                if (connection.State == System.Data.ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return null;
        }

        #endregion
        

    }
}
